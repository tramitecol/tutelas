La tutela es el derecho, la responsabilidad o la autoridad que se obtiene para cuidar de una persona menor de edad o que no puede valerse por sí misma, como los discapacitados, y de sus bienes.

La tutela proporciona refugio, amparo, protección y asistencia y se produce cuando los niños son huérfanos, no tienen padres o no tienen familia.

La tutela también se utiliza para referirse a un territorio que se confía a otro país o a las Naciones Unidas, se dice que el país está bajo tutela, bajo protección y cuidado.

La autoridad ejecutiva de la tutela es el tutor , que tiene el poder de representar al niño. El concepto de tutela consiste en las acciones, funciones o deberes de un tutor o custodio legal. Una persona puede estar incapacitada para ejercer la tutela de otra, por ejemplo, por haber sido excluida directamente de ese cargo por sus padres mediante un testamento, por no poder ser tutor a causa de una enfermedad, por haber sido condenada por un delito contra la familia o por estar cumpliendo una pena.

La tutela de alguien sobre una persona finaliza por decreto judicial, por la restitución de la patria potestad al progenitor o progenitores, por el fallecimiento de la persona, por la adopción de un menor o por la mayoría de edad de éste.

La tutela puede ser concedida a alguien por ley o por testamento y puede ser para administrar los bienes y dirigir o cuidar a un menor, así como para representar y afirmar que siempre se vela por él, que recibe apoyo, protección, defensa, etc.

https://tramitecol.com.co/derecho-de-peticion/desacato-tutela/